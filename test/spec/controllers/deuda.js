'use strict';

describe('Controller: DeudaCtrl', function () {

  // load the controller's module
  beforeEach(module('servipagApp'));

  var DeudaCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DeudaCtrl = $controller('DeudaCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(DeudaCtrl.awesomeThings.length).toBe(3);
  });
});
