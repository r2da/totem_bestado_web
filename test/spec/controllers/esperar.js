'use strict';

describe('Controller: EsperarCtrl', function () {

  // load the controller's module
  beforeEach(module('servipagApp'));

  var EsperarCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EsperarCtrl = $controller('EsperarCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(EsperarCtrl.awesomeThings.length).toBe(3);
  });
});
