'use strict';

describe('Controller: CredencialesCtrl', function () {

  // load the controller's module
  beforeEach(module('servipagApp'));

  var CredencialesCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CredencialesCtrl = $controller('CredencialesCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(CredencialesCtrl.awesomeThings.length).toBe(3);
  });
});
