'use strict';

describe('Controller: MailCtrl', function () {

  // load the controller's module
  beforeEach(module('servipagApp'));

  var MailCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MailCtrl = $controller('MailCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(MailCtrl.awesomeThings.length).toBe(3);
  });
});
