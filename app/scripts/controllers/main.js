'use strict';

/**
 * @ngdoc function
 * @name servipagApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the servipagApp
 */
angular.module('servipagApp')
    .controller('MainCtrl', function($scope, $rootScope) {
        $scope.servicio = 0;
        $scope.cuenta = '';

        $scope.ObtenerEstado = function(servicio) {
            $rootScope.servicio = servicio;
            location.hash = '/ingresar';
        }
    });