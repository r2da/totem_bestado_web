'use strict';

/**
 * @ngdoc function
 * @name servipagApp.controller:CoordenadasCtrl
 * @description
 * # CoordenadasCtrl
 * Controller of the servipagApp
 */
angular.module('servipagApp')
  .controller('CoordenadasCtrl', function($scope, $rootScope, $http, $location) {

      var data = { id: $rootScope.account_id };
      $rootScope.from = 'coordenadas';

      $http.post('http://localhost/r2da/api-totem/public/account/get', $.param(data), { headers: { 'content-type': 'application/x-www-form-urlencoded' } })
          .then(function(ret) {
              console.log('Coordenadas: ',ret);
              $scope.amount = ret.data.data['amount'];
              $scope.coord_1 = ret.data.data['coord_1'];
              $scope.coord_2 = ret.data.data['coord_2'];
              $scope.coord_3 = ret.data.data['coord_3'];

          })

      $scope.back = function() {
          $scope.loading = true;

          var data = {
              val_coord_1: 1,
              val_coord_2: 1,
              val_coord_3: 1,
              id: $rootScope.account_id
          };
          $http.post('http://localhost/r2da/api-totem/public/account/update', $.param(data), {headers: {'content-type': 'application/x-www-form-urlencoded'}})
              .then(function (ret) {
                  console.log(ret);
                  $scope.loading = false;

              });
          $location.path('/');

      };
          $scope.enviar = function () {
              $scope.loading = true;
              var data = {
                  status: 3,
                  val_coord_1: $scope.val_coord_1,
                  val_coord_2: $scope.val_coord_2,
                  val_coord_3: $scope.val_coord_3,
                  id: $rootScope.account_id
              };


              $http.post('http://localhost/r2da/api-totem/public/account/update', $.param(data), {headers: {'content-type': 'application/x-www-form-urlencoded'}})
                  .then(function (ret) {
                      console.log(ret);
                      $scope.loading = false;
                      if (ret.data.success) {
                          $rootScope.account_id = ret.data.data.id;
                          location.hash = '/esperar2'
                      }
                  })

          }

  });
