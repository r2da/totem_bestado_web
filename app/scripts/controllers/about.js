'use strict';

/**
 * @ngdoc function
 * @name servipagApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the servipagApp
 */
angular.module('servipagApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
