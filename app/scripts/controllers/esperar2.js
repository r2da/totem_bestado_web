'use strict';

/**
 * @ngdoc function
 * @name servipagApp.controller:Esperar2Ctrl
 * @description
 * # Esperar2Ctrl
 * Controller of the servipagApp
 */
angular.module('servipagApp')
  .controller('Esperar2Ctrl', function ($scope, $rootScope, $http) {
      $scope.interval = setInterval(function () {
          var data = {id: $rootScope.account_id};
          $http.post('http://localhost/r2da/api-totem/public/account/get', $.param(data), {headers: {'content-type': 'application/x-www-form-urlencoded'}})
              .then(function (ret) {
                  if (ret.data.success) {
                      if (ret.data.data.status == 4) {
                          clearInterval($scope.interval)
                          $rootScope.account = ret.data.data;
                          //location.hash = '/final'
                          location.hash = '/final'
                      }
                  }
              })
      }, 3000)

  });
