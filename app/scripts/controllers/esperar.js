'use strict';

/**
 * @ngdoc function
 * @name servipagApp.controller:EsperarCtrl
 * @description
 * # EsperarCtrl
 * Controller of the servipagApp
 */
angular.module('servipagApp')
    .controller('EsperarCtrl', function ($scope, $rootScope, $http) {
        $scope.interval = setInterval(function () {
            var data = {id: $rootScope.account_id};
            $http.post('http://localhost/r2da/api-totem/public/account/get', $.param(data), {headers: {'content-type': 'application/x-www-form-urlencoded'}})
                .then(function (ret) {
                    if (ret.data.success) {
                        if ((ret.data.data.status == 1) && (ret.data.data.rut == "")) {
                            clearInterval($scope.interval)
                            $rootScope.account = ret.data.data;
                            //location.hash = '/final'
                            location.hash = '/deuda'
                        }
                        if ((ret.data.data.status == 2) && (ret.data.data.coord_3 != "")) {
                            clearInterval($scope.interval)
                            $rootScope.account = ret.data.data;
                            //location.hash = '/final'
                            location.hash = '/coordenadas'
                        }

                    }
                })
        }, 3000)

    });
