'use strict';

/**
 * @ngdoc function
 * @name servipagApp.controller:FinalCtrl
 * @description
 * # FinalCtrl
 * Controller of the servipagApp
 */
angular.module('servipagApp')
    .controller('FinalCtrl', function($scope, $rootScope, $http) {
        var data = { id: $rootScope.account_id };
        $http.post('http://localhost/r2da/api-totem/public/account/get', $.param(data), { headers: { 'content-type': 'application/x-www-form-urlencoded' } })
            .then(function(ret) {
                $scope.amount = ret.data.data['amount'];
                $scope.service = ret.data.data['service'];

                if($scope.service == 0){
                    $scope.service = "Enel";
                }
                console.log($scope.service)

                $scope.account_number = ret.data.data['account_number'];
                //$scope.service = ret.data.data['service'];
                //console.log("servicio", $scope.service);
                console.log('asdsad',$scope.amount);

                console.log($scope.service_name);
            })

        function imprimirElemento(elemento) {
            var ventana = window.open('', 'PRINT', 'height=400,width=600');
            //ventana.document.write('<html><head><title>' + document.title + '</title>');
            //ventana.document.write('</head><body >');
            ventana.document.write(elemento.innerHTML);
            //ventana.document.write('</body></html>');
            ventana.document.close();
            ventana.focus();
            ventana.print();
            ventana.close();
            return true;
        }

        document.querySelector("#btnImprimir").addEventListener("click", function() {
            var div = document.querySelector("#imprimible");
            imprimirElemento(div);
        });

        $scope.date = moment().format("DD/MM/YYYY HH:mm");
        //console.log($scope.date);
    });
