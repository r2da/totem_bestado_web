'use strict';

/**
 * @ngdoc function
 * @name servipagApp.controller:DeudaCtrl
 * @description
 * # DeudaCtrl
 * Controller of the servipagApp
 */
angular.module('servipagApp')
  .controller('DeudaCtrl', function ($scope, $rootScope, $http, $location) {
      var data = { id: $rootScope.account_id };
      $scope.enviar = function() {
          var data = {id: $rootScope.account_id};
          $http.post('http://localhost/r2da/api-totem/public/account/get', $.param(data), {headers: {'content-type': 'application/x-www-form-urlencoded'}})
              .then(function (ret) {
                  if (ret.data.success) {
                      if ((ret.data.data.status == 1) && (ret.data.data.service == 2)) {

                          $rootScope.account = ret.data.data;
                          //location.hash = '/final'
                          location.hash = '/mail'
                      }else{location.hash = '/credenciales'}
                  }

              })





      }

      $http.post('http://localhost/r2da/api-totem/public/account/get', $.param(data), { headers: { 'content-type': 'application/x-www-form-urlencoded' } })
          .then(function(ret) {
              console.log(ret);
              $scope.amount = ret.data.data['amount'];

          })


      $scope.back = function() {
          $scope.loading = true;
          var data = {
              pass: 1,
              id: $rootScope.account_id
          };
          $http.post('http://localhost/r2da/api-totem/public/account/update', $.param(data), {headers: {'content-type': 'application/x-www-form-urlencoded'}})
              .then(function (ret) {
                  console.log(ret);
                  $scope.loading = false;
                  $location.path('/');
              })
      }

  });
