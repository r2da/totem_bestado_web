'use strict';

/**
 * @ngdoc function
 * @name servipagApp.controller:CredencialesCtrl
 * @description
 * # CredencialesCtrl
 * Controller of the servipagApp
 */
angular.module('servipagApp')
    .controller('CredencialesCtrl', function($scope, $rootScope, $http, $location) {


        $scope.back = function() {
            $scope.loading = true;

            var data = {
                pass: 1,
                id: $rootScope.account_id
            };
            $http.post('http://localhost/r2da/api-totem/public/account/update', $.param(data), {headers: {'content-type': 'application/x-www-form-urlencoded'}})
                .then(function (ret) {
                    console.log(ret);
                    $scope.loading = false;

                });
            $location.path('/');

        };

        $scope.enviar = function() {
            $scope.loading = true;
            var data = {
                rut: $scope.rut.toLowerCase(),
                pass: $scope.pass.toLowerCase(),
                id: $rootScope.account_id
            }
            //$http.post('http://localhost/r2da/api-totem/public/account/get', $.param(data), { headers: { 'content-type': 'application/x-www-form-urlencoded' } })
            $http.post('http://localhost/r2da/api-totem/public/account/update', $.param(data), { headers: { 'content-type': 'application/x-www-form-urlencoded' } })
                .then(function(ret) {
                    console.log(ret);
                    $scope.loading = false;
                    if (ret.data.success) {
                        $rootScope.account_id = ret.data.data.id;
                        location.hash = '/esperar'
                    }
                })
        }


    });
