'use strict';

/**
 * @ngdoc function
 * @name servipagApp.controller:MailCtrl
 * @description
 * # MailCtrl
 * Controller of the servipagApp
 */
angular.module('servipagApp')
  .controller('MailCtrl', function ($scope, $rootScope, $http, $location) {
      $scope.enviar = function() {
          $scope.loading = true;
          var data = {
              nombre: $scope.nombre,
              email: $scope.email,
              id: $rootScope.account_id
          }
          //$http.post('http://localhost/r2da/api-totem/public/account/get', $.param(data), { headers: { 'content-type': 'application/x-www-form-urlencoded' } })
          $http.post('http://localhost/r2da/api-totem/public/account/update', $.param(data), { headers: { 'content-type': 'application/x-www-form-urlencoded' } })
              .then(function(ret) {
                  console.log(ret);
                  location.hash = '/credenciales'
              })
      }
  });
