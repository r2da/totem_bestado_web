'use strict';

/**
 * @ngdoc function
 * @name servipagApp.controller:IngresarCtrl
 * @description
 * # IngresarCtrl
 * Controller of the servipagApp
 */
angular.module('servipagApp')
    .controller('IngresarCtrl', function($scope, $rootScope, $http) {
        $rootScope.account_id = false;
        $scope.servicio = angular.copy($rootScope.servicio || 0);
        $scope.loading = false;
        $scope.enviar = function() {
            $scope.loading = true;
            var data = {
                    account_number: $scope.cuenta,
                    service: $scope.servicio
                }
                //console.log($scope.cuenta);
            $http.post('http://localhost/r2da/api-totem/public/account/add', $.param(data), { headers: { 'content-type': 'application/x-www-form-urlencoded' } })
                .then(function(ret) {
                    console.log(ret);
                    $scope.loading = false;
                    if (ret.data.success) {
                        $rootScope.account_id = ret.data.data.id;
                        location.hash = '/esperar'
                    }
                })
        }
    });
