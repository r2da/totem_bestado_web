'use strict';

/**
 * @ngdoc overview
 * @name servipagApp
 * @description
 * # servipagApp
 *
 * Main module of the application.
 */
angular
    .module('servipagApp', [
        'ngAnimate',
        'ngCookies',
        'alexjoffroy.angular-loaders',
        //'ngResource',
        'ngRoute'

        //'ngSanitize',
        //'ngTouch'
    ])
    .config(function($routeProvider, $locationProvider) {
        $locationProvider.hashPrefix('');
        $routeProvider
            .when('/', {
                templateUrl: 'views/main.html',
                controller: 'MainCtrl',
                controllerAs: 'main'
            })
            .when('/about', {
                templateUrl: 'views/about.html',
                controller: 'AboutCtrl',
                controllerAs: 'about'
            })
            .when('/ingresar', {
                templateUrl: 'views/ingresar.html',
                controller: 'IngresarCtrl',
                controllerAs: 'ingresar'
            })
            .when('/esperar', {
                templateUrl: 'views/esperar.html',
                controller: 'EsperarCtrl',
                controllerAs: 'esperar'
            })
            .when('/final', {
              templateUrl: 'views/final.html',
              controller: 'FinalCtrl',
              controllerAs: 'final'
            })
            .when('/credenciales', {
              templateUrl: 'views/credenciales.html',
              controller: 'CredencialesCtrl',
              controllerAs: 'credenciales'
            })
            .when('/deuda', {
              templateUrl: 'views/deuda.html',
              controller: 'DeudaCtrl',
              controllerAs: 'deuda'
            })
            .when('/coordenadas', {
              templateUrl: 'views/coordenadas.html',
              controller: 'CoordenadasCtrl',
              controllerAs: 'coordenadas'
            })
            .when('/mail', {
              templateUrl: 'views/mail.html',
              controller: 'MailCtrl',
              controllerAs: 'mail'
            })
            .when('/esperar2', {
              templateUrl: 'views/esperar2.html',
              controller: 'Esperar2Ctrl',
              controllerAs: 'esperar2'
            })
            .otherwise({
                redirectTo: '/'
            });
    })